import React, { Component } from "react";
import { BrowserRouter, Route, Switch} from "react-router-dom";

import Inicio from "../paginas/inicio";
import Postagens from "../paginas/postagens";
import NadaEncontrado from "../paginas/404";

export default class extends Component {
    render(){
        return(
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Inicio}/>
                    <Route path="/postagens" component={Postagens}/>
                    <Route component={NadaEncontrado}/>
                </Switch>
            </BrowserRouter>
        )
    }
}