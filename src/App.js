import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { AppBar, Drawer, List, ListItem } from 'material-ui';
import './App.css';
import Rotas from "./rotas";

injectTapEventPlugin();

class App extends Component {
  
  constructor(){
    super();
    this.state = {
      drawerOpened : false
    };
  };
  _toggleDrawer(){
    this.setState({
      drawerOpened : !this.state.drawerOpened
    });
  };
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <AppBar title='Ere' onLeftIconButtonClick={() => this._toggleDrawer()} />
          <Drawer open={this.state.drawerOpened} docked={false} onRequestChange={() => this._toggleDrawer()}>
            <List>
              <a href="/">
                <ListItem>INICIO</ListItem>
              </a>
              <a href="/postagens">
                <ListItem>POSTAGENS</ListItem>
              </a>
            </List>
          </Drawer>
          <Rotas />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
